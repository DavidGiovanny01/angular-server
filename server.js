var express = require('express');

const app = express(),
    staticApp = express.static('public'),
    PORT = process.env.PORT || 8000;

app.use('/', staticApp);
app.use('*', staticApp);

app.listen(PORT);
console.log(`App running on port ${PORT}`);